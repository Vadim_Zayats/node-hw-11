import { sequelize } from "./db";
import { DataTypes } from "sequelize";
import { argv } from "process";

const NewsPost = sequelize.define("newsPost", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.TEXT,
  },
  text: {
    type: DataTypes.TEXT,
  },
  created_date: {
    type: DataTypes.DATE,
  },
});

(async () => {
  // Skip first two elements (node executable and script file path)
  const [, , title, text] = argv;
  const newPost = await NewsPost.create({
    title,
    text,
    created_date: new Date(),
  });
  console.log("New post created:", newPost);
  process.exit(0);
})();
