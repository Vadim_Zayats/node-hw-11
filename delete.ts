import { sequelize } from "./db";
import { DataTypes } from "sequelize";
import { argv } from "process";

const NewsPost = sequelize.define("newsPost", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.TEXT,
  },
  text: {
    type: DataTypes.TEXT,
  },
  created_date: {
    type: DataTypes.DATE,
  },
});

(async () => {
  const postId = argv[2];
  await NewsPost.destroy({
    where: {
      id: postId,
    },
  });
  console.log("Post deleted");
  process.exit(0);
})();
