import { sequelize } from "./db";
import { DataTypes } from "sequelize";
import { argv } from "process";

const NewsPost = sequelize.define("newsPost", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.TEXT,
  },
  text: {
    type: DataTypes.TEXT,
  },
  created_date: {
    type: DataTypes.DATE,
  },
});

(async () => {
  const [_, __, id, title, text] = argv;
  await NewsPost.update(
    {
      title,
      text,
    },
    {
      where: {
        id,
      },
    }
  );
  console.log("Post updated");
  process.exit(0);
})();
