import { sequelize } from "./db";
import { DataTypes } from "sequelize";

const NewsPost = sequelize.define("newsPost", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.TEXT,
  },
  text: {
    type: DataTypes.TEXT,
  },
  created_date: {
    type: DataTypes.DATE,
  },
});

(async () => {
  const posts = await NewsPost.findAll();
  console.log(posts);
  process.exit(0);
})();
